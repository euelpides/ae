; vi: ft=nasm

%include 'macros.inc'

align 16

default rel
%stacksize flat64

file_buffer_max_size   equ 16777216
STATE_QUIT             equ 0
STATE_RUNNING          equ 1

section .bss
file_buffer:      resb file_buffer_max_size
file_line_total:  resd 1
infile_fd:        resd 1
outfile_fd:       resd 1
cmdl_buffer:      resb 2048
cmdl_buffer_size: resq 1
readl_buffer:     resb 1024
readl_total:      resd 1
state:            resb 1
current_line:     resd 1
change_buffer:    resb 1024

section .data
saveas_error_msg: db 'error: saveas: parameter required',10,0
saveas_error_msg_len equ $-saveas_error_msg

section .text

; *****
; void main(int argc,char**argv)
; open and copy specified file
; to file_line_total-buffer file_buffer and
; execute instructions from the user
; *****
global main
main:
	prolog 64
%define argc -4
%define argv -12
%define running -16

	mov [rbp+argc],edi
	mov [rbp+argv],rsi
	mov dword[rbp+running],1
	mov dword[file_line_total],0
	mov byte[state],STATE_RUNNING
	mov dword[current_line],0

; require 3 arguments (progname INFILE OUTFILE)
	mov edi,[rbp+argc]
	cmp edi,2
	jl .exit_error

; open INFILE
	mov rdi,[rbp+argv]
	open [rdi+8],O_RDONLY
	mov [infile_fd],eax
	cmp eax,0
	jl .exit_error

; read entire INFILE file_line_total-buffered
xor ecx,ecx
.a01:
	mov edi,[infile_fd]
	mov eax,[file_line_total]
	inc dword[file_line_total]
	shl eax,10
	lea rsi,[file_buffer+eax]
	call readl
	cmp eax,0
	jz .a01
.a02:

; without OUTFILE, use stdout
	mov edi,[rbp+argc]
	cmp edi,3
	jae .l01
	mov dword[outfile_fd],1
	jmp .l02

; create/open OUTFILE
.l01:
	mov rdi,[rbp+argv]
	open [rdi+16],O_WRONLY|O_CREAT,0x1a4
	mov [outfile_fd],eax
	cmp eax,0
	jl .exit_error
.l02:

	; main loop
.l03:
	call getcmdl
	inc qword[cmdl_buffer_size] ; include endline

	lea rdi,[cmdl_buffer]
	call interpret_cmdl

	cmp byte[state],STATE_QUIT
	jz .l10

	jmp .l03

.l10:
.exit_ok:
	epilog
	exit 0

.exit_error:
	epilog
	exit 1
%undef argc
%undef argv
%undef running

; *****
; void getcmdl()
; read until endline
; terminate with NULL byte
; *****
global getcmdl
getcmdl:
	prolog 64
%define n -8

	; clear cmdl_buffer
	mov rcx,2048/64
.l00:
	mov qword[cmdl_buffer+rcx],0
	loop .l00

	; read single line from stdin
	mov qword[rbp+n],0
.l01:
	mov rcx,[rbp+n]
	lea rsi,[cmdl_buffer+rcx]
	read 0,1

	; loop checks
	cmp eax,0
	jbe .l02b

	mov rcx,[rbp+n]
	cmp byte[cmdl_buffer+rcx],10
	jz .l02

	; loop update
	inc qword[rbp+n]
	jmp .l01

	; append quit message at end of input
.l02b:
	mov rcx,[rbp+n]
	lea rsi,[cmdl_buffer+rcx]
	mov byte[rsi],'q'

	inc qword[rbp+n]

	mov rcx,[rbp+n]
	lea rsi,[cmdl_buffer+rcx]
	mov byte[rsi],0

.l02:

	mov rcx,[rbp+n]
	mov [cmdl_buffer_size],rcx
	xor eax,eax
	epilog
	ret
%undef n

; *****
; void interpret_cmdl(char*cmdline)
; read null-terminated command
; from buffer
; *****
global interpret_cmdl
interpret_cmdl:
	prolog 64
%define buffer -8
%define ch -12

	mov [rbp+buffer],rdi

	; find correct command
	mov rdi,[rbp+buffer]
.l01:

	; quit command
.l01q:
	cmp byte[rdi],'q'
	jnz .l01qe
	mov byte[state],STATE_QUIT
	jmp .l02
.l01qe:

	; print entire buffer
.l01p:
	cmp byte[rdi],','
	jnz .l01pe
	mov edi,1
	call print_file_buffer
	jmp .l02
.l01pe:

	; saveas command
.l01w:
	cmp byte[rdi],'w'
	jnz .l01we
	add rdi,2 ; ignore the "w " in "w [a-zA-Z0-9_\.]\+"
	call saveas
	jmp .l02
.l01we:

	; change line command
.l01c:
	cmp byte[rdi],'c'
	jnz .l01ce
	mov edi,[current_line]
	call changeline
	jmp .l02
.l01ce:

	; print current_line
.l01l:
	cmp byte[rdi],'n'
	jnz .l01le
	mov edi,[current_line]
	call printline
	jmp .l02
.l01le:

	; numerical character
.l01n:
	cmp byte[rdi],'0'
	jl .l01ne
	cmp byte[rdi],'9'
	jg .l01ne

	mov rdi,[rbp+buffer]
	call s2i
	mov dword[current_line],eax
	mov edi,[current_line]
	call printline
	jmp .l02
.l01ne:

	; unrecognized command
.l01b:
	mov byte[rbp+ch],`?`
	mov byte[rbp+ch+1],10
	lea rsi,[rbp+ch]
	write 1,2
	jmp .l02

.l02:
	epilog
	ret
%undef buffer
%undef ch

; *****
; int readl(int fd,char*dest)
; read entire file_line_total from fd
; and copy to buffer dest
; *****
global readl
readl:
	prolog 64
%define fd -4
%define count -8
%define nread -12
%define dest -20
%define i -24

	mov dword[rbp+count],0
	mov dword[rbp+nread],0
	mov dword[rbp+i],0

	mov [rbp+fd],edi
	cmp edi,0
	jl .l49

	mov [rbp+dest],rsi
	cmp rsi,0
	jl .l49

; read until endline
.l40:

; init
	xor rcx,rcx
.l41:

	; update
	mov ecx,[rbp+count]
	inc dword[rbp+count]
	inc dword[readl_total]
	lea rsi,[ecx+readl_buffer]
	read [rbp+fd],1
	mov [rbp+nread],eax

	; check
	cmp eax,0 ;check
	jle .l49

	cmp dword[rbp+count],1023
	jg .l49

	mov ecx,[rbp+count]
	lea rsi,[readl_buffer+ecx-1]
	cmp byte[rsi],10
	jz .l41b

	jmp .l41

.l41b:
	; copy to [rbp+dest]
	mov dword[rbp+i],0
	inc dword[rbp+count]
.l42:
	mov rdi,[rbp+dest]
	mov r8d,[rbp+i]
	mov al,[readl_buffer+r8d]
	mov byte[rdi+r8],al

	inc dword[rbp+i] ; update/check
	mov r8d,[rbp+i]
	cmp r8d,[rbp+count]
	jz .l48
	cmp al,10
	jz .l43
	jmp .l42

	; copy endline
.l43:
	mov rdi,[rbp+dest]
	mov r8d,[rbp+i]
	inc r8d
	mov al,10
	mov byte[rdi+r8],al

.l48:
	epilog
	xor eax,eax
	ret

; return error
.l49:
	epilog
	mov eax,1
	ret
%undef fd
%undef count
%undef nread
%undef dest
%undef i

; *****
; void printline(int line)
; print specified line from
; file buffer
; *****
global printline
printline:
%define i -4
%define line -8
	prolog 64

	mov dword[rbp+line],edi
	mov dword[rbp+i],0

.l01:
	mov eax,[rbp+line]
	shl eax,10
	add eax,[rbp+i]
	lea rsi,[file_buffer+eax]
	write 1,1

	cmp byte[file_buffer+eax],10
	jz .l02

	inc dword[rbp+i]
	cmp dword[rbp+i],1024
	jl .l01

.l02:
	epilog
	ret
%undef i
%undef line

; *****
; void print_file_buffer(int output_fd)
; print entire file stored
; in file_line_total-buffer file_buffer
; *****
global print_file_buffer
print_file_buffer:
	prolog 64
%define i -4
%define j -8
%define fd -16

	mov dword[rbp+fd],edi

	mov dword[rbp+i],0 ; init outer loop
.l03:

	mov dword[rbp+j],0 ; init inner loop
.l05:

	mov eax,[rbp+i]
	shl eax,10
	add eax,[rbp+j]
	lea rsi,[file_buffer+eax]
	cmp byte[file_buffer+eax],0
	jz .l06
	write dword[rbp+fd],1

	inc dword[rbp+j]
	mov eax,[rbp+j]
	cmp eax,[readl_total]
	jge .l06
	mov eax,[rbp+i]
	shl eax,10
	add eax,[rbp+j]
	dec eax
	cmp byte[file_buffer+eax],10
	jz .l06
	jmp .l05

.l06:

	inc dword[rbp+i]
	mov eax,[rbp+i]
	cmp eax,[file_line_total]
	jl .l03

	epilog
	ret
%undef i
%undef j
%undef fd

; *****
; int s2i(char*str)
; convert string to integer
; *****
s2i:
	prolog 64
%define s -8
%define v -12
%define l -16
%define p -20

	mov qword[rbp+s],rdi
	mov dword[rbp+v],0
	mov dword[rbp+l],0
	mov dword[rbp+p],1

		; count numeric characters
		; starting at address in [rbp+s]
		jmp .l02		; pretest jump
.l01:	inc dword[rbp+l]	; update
.l02:	mov eax,[rbp+l]		; check
		mov rdi,[rbp+s]
		mov al,[rdi+rax]
		cmp al,'0'
		jl .l03
		cmp al,'9'
		jg .l03
		jmp .l01
.l03:

		; calculate integer
		jmp .l05 ; pretest jump
.l04:	mov rdi,[rbp+s]		;update
		mov eax,[rbp+l]
		mov al,[rdi+rax]
		sub al,'0'
		xor rdx,rdx
		mov ecx,10
		div ecx
		mov eax,edx
		mov ecx,[rbp+p]
		mul ecx
		add [rbp+v],eax
		mov eax,[rbp+p]
		mov ecx,10
		mul ecx
		mov [rbp+p],eax
.l05:	mov eax,[rbp+l]
		dec dword[rbp+l]
		cmp eax,0		;check
		jnz .l04

	mov eax,[rbp+v]

	epilog
	ret
%undef s
%undef v
%undef l
%undef p

; *****
; void saveas(char*name)
; write file buffer to name
; *****
global saveas
saveas:
	prolog 64
%define name -8
%define total -12
%define fd -16

	mov [rbp+name],rdi
	mov dword[rbp+total],0

	; calculate string length
	xor rcx,rcx
.l01:
	lea rax,[rcx]
	inc rcx
	cmp byte[rdi+rax],10
	jg .l01
.l02:
	mov [rbp+total],ecx

	cmp dword[rbp+total],1
	jle .e01

	; Add null-terminator
.l03:
	mov byte[rdi+rax],0

	; create file
	open qword[rbp+name],O_WRONLY|O_CREAT,0q644
	mov dword[rbp+fd],eax
	cmp eax,-1
	jz .e02

	; write entire buffer to file
	mov edi,[rbp+fd]
	call print_file_buffer

	close dword[rbp+fd]

	jmp .e02
.e01:
	write 1,saveas_error_msg,saveas_error_msg_len
.e02:
	epilog
	ret
%undef name
%undef total
%undef fd

; *****
; void changeline(int line)
; read line from stdin and
; replace numbered line in
; file buffer
; *****
global changeline
changeline:
	prolog 64
%define line -4

	mov [rbp+line],edi

	; clear change_buffer
	mov rcx,2048/64
.l00:
	mov qword[change_buffer+rcx],0
	loop .l00

	xor edi,edi
	lea rsi,[change_buffer]
	call readl

	xor r8,r8
.l01:
	lea rsi,[change_buffer+r8]
	write 1,1
	mov al,[change_buffer+r8]
	inc r8
	cmp al,10
	jnz .l01

	epilog
	ret
