CC=       cc
CFLAGS=   -Wfatal-errors -g
LDFLAGS=  -no-pie -z noexecstack
ASMFLAGS= -felf64 -F dwarf -g -l $(basename $(@)).lst

all: ae
main.o: main.asm macros.inc
	nasm $(ASMFLAGS) $<
%.o: %.asm
	nasm $(ASMFLAGS) $<
ae: main.o macros.inc
	$(CC) $< -o $@ $(LDFLAGS)
clean:
	$(RM) *.o *.lst ae
