O_RDONLY          equ 0x00
O_WRONLY          equ 0x01
O_RDWR            equ 0x02
O_CREAT           equ 0x40
SEEK_SET          equ 0x0
SEEK_CUR          equ 0x1
SEEK_END          equ 0x2

; rsi (buffer) required here
%macro read 3
	mov eax,0
	mov edi,%1
	mov rsi,%2
	mov edx,%3
	syscall
%endmacro

; rsi (buffer) provided elsewhere
%macro read 2
	mov eax,0
	mov edi,%1
	mov edx,%2
	syscall
%endmacro

; rsi (buffer) required here
%macro write 3
	mov eax,1
	mov edi,%1
	mov rsi,%2
	mov edx,%3
	syscall
%endmacro

; rsi (buffer) provided elsewhere
%macro write 2
	mov eax,1
	mov edi,%1
	mov edx,%2
	syscall
%endmacro

; rdx (mode) required here
%macro open 3
	mov eax,2
	mov rdi,%1
	mov rsi,%2
	mov rdx,%3
	syscall
%endmacro

; rdx (mode) not required
%macro open 2
	mov eax,2
	mov rdi,%1
	mov rsi,%2
	syscall
%endmacro

%macro close 1
	mov eax,3
	mov edi,%1
	syscall
%endmacro

%macro exit 1
	mov rax,60
	mov edi,%1
	syscall
%endmacro

%macro lseek 3
	mov rax,8
	mov edi,%1
	mov rsi,%2
	mov rdx,%3
	syscall
%endmacro

%macro prolog 1
	push rbp
	mov rbp,rsp
	sub rsp,%1
%endmacro

%macro epilog 0
	mov rsp,rbp
	pop rbp
%endmacro

%macro repeat 0
	%push
	%$begin:
%endmacro

%macro until 1
	j%1 %$begin
	%pop
%endmacro
